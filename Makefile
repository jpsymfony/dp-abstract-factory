build_docker_linux: build_linux up_linux install

build_docker_mac: build_mac up_mac composer

build_linux:
	docker-compose build --build-arg USER=${USER} --build-arg UID=${UID} php

build_mac:
	docker-compose build php

up_linux:
	docker-compose up -d

up_mac:
	-docker-sync start
	docker-compose up -d

ssh:
	docker exec -it php_abstract_factory bash

install: composer

composer:
	docker exec -i php_abstract_factory bash -c 'composer install --no-interaction -o'

setfacl:
	setfacl -dR -m u:"www-data":rwX -m u:$(whoami):rwX var
	setfacl -R -m u:"www-data":rwX -m u:$(whoami):rwX var

stop_linux:
	-docker-compose stop

stop_mac:
	-docker-sync stop
	docker-sync-stack clean
	docker image prune --force && docker container prune --force

cache:
	docker exec -i php_abstract_factory bash -c 'bin/console cache:clear --no-warmup && bin/console cache:warmup'

cache_all:
	docker exec -i php_abstract_factory bash -c 'bin/console cache:clear --no-warmup --env dev && bin/console cache:warmup --env dev && bin/console cache:clear --no-warmup --env=test && bin/console cache:warmup --env=test && bin/console cache:clear --no-warmup --env prod && bin/console cache:warmup --env prod'

clean_docker:
	docker image prune --force && docker container prune --force

test_phpunit:
	docker exec -it php_abstract_factory bash -c 'vendor/bin/phpunit'

tests: test_phpunit test_behat

.PHONY: build up ssh install composer setfacl stop cache cache_all clean_docker behat phpunit tests
