FROM php:7.4-apache
ARG USER
ARG UID

# PHP extensions
ENV APCU_VERSION 5.1.18

RUN apt-get update && apt-get upgrade -y && apt-get install -y \
      nano \
      git \
      libtool \
      locales \
      locales-all \
      apt-utils \
      g++ \
      pkg-config \
      cmake \
      protobuf-compiler \
      autoconf \
      libicu-dev \
      zlib1g-dev \
      libzip-dev \
      libxml2 \
      libxml2-dev \
      libreadline-dev \
      unzip \
      libssl-dev \
      apt-transport-https \
      libpng-dev \
      libjpeg62-turbo-dev \
      libfreetype6-dev \
      libmagickwand-dev \
      libxslt-dev \
      apt-transport-https \
      software-properties-common \
      libmagick++-dev \
      libmagickwand-dev \
      acl \
      bison \
      build-essential \
      libonig-dev

ENV LANG fr_FR.UTF-8
ENV LC_ALL fr_FR.UTF-8

RUN docker-php-ext-configure pdo_mysql --with-pdo-mysql=mysqlnd
RUN docker-php-ext-configure intl
RUN docker-php-ext-install pdo_mysql
RUN docker-php-ext-install sockets
RUN docker-php-ext-configure intl
RUN docker-php-ext-install intl
RUN docker-php-ext-install zip
RUN docker-php-ext-install xsl
RUN docker-php-ext-install bcmath
RUN docker-php-ext-install pcntl
RUN docker-php-ext-install bz2
RUN docker-php-ext-install json
RUN docker-php-ext-configure gd --with-jpeg=/usr/include/ --with-freetype=/usr/include/
RUN docker-php-ext-install -j$(nproc) gd
RUN docker-php-source delete

RUN pecl install xdebug && docker-php-ext-enable xdebug
RUN pecl install apcu-$APCU_VERSION && docker-php-ext-enable --ini-name 05-opcache.ini opcache && docker-php-ext-enable --ini-name 20-apcu.ini apcu

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
    && composer global require hirak/prestissimo \
    && chmod +x /usr/local/bin/composer || /bin/true \
    && composer self-update

COPY config/apache_project.conf /etc/apache2/sites-available/apache_project.conf
COPY config/php.ini /usr/local/etc/php/
COPY config/xdebug.ini /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

RUN echo 'alias sf="bin/console"' >> ~/.bashrc
RUN echo 'alias behat="vendor/bin/behat"' >> ~/.bashrc
RUN echo 'alias phpunit="vendor/bin/phpunit"' >> ~/.bashrc

RUN a2dissite 000-default
RUN a2ensite apache_project
RUN a2enmod rewrite

# Xdebug with PHPUnit
ENV PHP_IDE_CONFIG serverName=abstract-factory

USER root

ADD scripts /scripts
RUN chmod +x /scripts/*.sh
CMD /scripts/run.sh

