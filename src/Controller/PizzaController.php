<?php

namespace App\Controller;

use App\Client\PizzeriaBrest;
use App\Client\PizzeriaStrasbourg;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PizzaController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function index(PizzeriaBrest $pizzeriaBrest, PizzeriaStrasbourg $pizzeriaStrasbourg)
    {
        $typesPizza = ['fromage', 'poivrons', 'fruitsDeMer', 'vegetarienne'];

        foreach ($typesPizza as $type) {
            $pizzaBrest = $pizzeriaBrest->commanderPizza($type);
            echo sprintf('<br/>Luc a commandé une %s', $pizzaBrest);

            $pizzaStrasbourg = $pizzeriaStrasbourg->commanderPizza($type);
            echo sprintf('<br/>Michel a commandé une %s', $pizzaStrasbourg);
        }

        return new Response();
    }
}
