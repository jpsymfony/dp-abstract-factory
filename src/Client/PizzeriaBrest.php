<?php

namespace App\Client;

use App\Entity\PizzaInterface;
use App\Exception\NotFoundPizzaException;

class PizzeriaBrest extends AbstractPizzeria
{
    public function creerPizza(string $name): PizzaInterface
    {
        /** @var PizzaInterface $pizza */
        foreach ($this->pizzas as $pizza) {
            if ($pizza->isTypeMatch($name)) {
                $pizza->setFabriqueIngredients($this->fabriqueIngredientsPizza);
                $pizza->setNom(sprintf('Pizza %s %s', $this->translator->trans($name), 'style Brest'));

                return $pizza;
            }
        }

        throw new NotFoundPizzaException(sprintf('None pizza found for name %s', $name));
    }
}