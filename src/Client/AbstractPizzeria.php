<?php

namespace App\Client;

use App\Entity\PizzaInterface;
use App\Factory\FabriqueIngredientsPizzaInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class AbstractPizzeria
{
    protected iterable $pizzas;

    protected TranslatorInterface $translator;

    public function __construct(iterable $pizzas, FabriqueIngredientsPizzaInterface $fabriqueIngredientsPizzaBrest, TranslatorInterface $translator)
    {
        $this->pizzas = $pizzas;
        $this->fabriqueIngredientsPizza = $fabriqueIngredientsPizzaBrest;
        $this->translator = $translator;
    }

    protected FabriqueIngredientsPizzaInterface $fabriqueIngredientsPizza;

    public function commanderPizza($name): PizzaInterface
    {
        $pizza = $this->creerPizza($name);

        $pizza->preparer();
        $pizza->cuire();
        $pizza->couper();
        $pizza->emballer();

        return $pizza;
    }

    protected abstract function creerPizza(string $name): PizzaInterface;
}