<?php

namespace App\Client;

use App\Entity\PizzaInterface;

class PizzeriaStrasbourg extends AbstractPizzeria
{
    public function creerPizza(string $name): PizzaInterface
    {
        /** @var PizzaInterface $pizza */
        foreach ($this->pizzas as $pizza) {
            if ($pizza->isTypeMatch($name)) {
                $pizza->setFabriqueIngredients($this->fabriqueIngredientsPizza);
                $pizza->setNom(sprintf('Pizza %s %s', $this->translator->trans($name), 'style Strasbourg'));

                return $pizza;
            }
        }

        throw new \Exception(sprintf('None pizza found for name %s', $name));
    }
}