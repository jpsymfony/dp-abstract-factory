<?php

Namespace App\Factory;

use App\Entity\Ingredients\Fromages\FromageInterface;
use App\Entity\Ingredients\Fromages\Regiano;
use App\Entity\Ingredients\Legumes\Ail;
use App\Entity\Ingredients\Legumes\Champignon;
use App\Entity\Ingredients\Legumes\Oignon;
use App\Entity\Ingredients\Moules\MouleInterface;
use App\Entity\Ingredients\Moules\MoulesFraiches;
use App\Entity\Ingredients\Pates\PateFine;
use App\Entity\Ingredients\Pates\PateInterface;
use App\Entity\Ingredients\Poivrons\PoivronInterface;
use App\Entity\Ingredients\Poivrons\PoivronRouge;
use App\Entity\Ingredients\Poivrons\PoivronsEnRondelles;
use App\Entity\Ingredients\Sauces\SauceInterface;
use App\Entity\Ingredients\Sauces\SauceMarinara;
use Doctrine\Common\Collections\ArrayCollection;

class FabriqueIngredientsPizzaBrest implements FabriqueIngredientsPizzaInterface
{
    public function creerPate(): PateInterface
    {
        return new PateFine();
    }

    public function creerSauce(): SauceInterface
    {
        return new SauceMarinara();
    }

    public function creerFromage(): FromageInterface
    {
        return new Regiano();
    }

    public function creerLegumes(): ArrayCollection
    {
        return new ArrayCollection([new Ail(), new Oignon(), new Champignon(), new PoivronRouge()]);
    }

    public function creerPoivrons(): PoivronInterface
    {
        return new PoivronsEnRondelles();
    }

    public function creerMoules(): MouleInterface
    {
        return new MoulesFraiches();
    }
}