<?php

Namespace App\Factory;

use App\Entity\Ingredients\Fromages\FromageInterface;
use App\Entity\Ingredients\Moules\MouleInterface;
use App\Entity\Ingredients\Pates\PateInterface;
use App\Entity\Ingredients\Poivrons\PoivronInterface;
use App\Entity\Ingredients\Sauces\SauceInterface;
use Doctrine\Common\Collections\ArrayCollection;

interface FabriqueIngredientsPizzaInterface
{
    public function creerPate(): PateInterface;

    public function creerSauce(): SauceInterface;

    public function creerFromage(): FromageInterface;

    public function creerLegumes(): ArrayCollection;

    public function creerPoivrons(): PoivronInterface;

    public function creerMoules(): MouleInterface;
}

