<?php

Namespace App\Factory;

use App\Entity\Ingredients\Fromages\FromageInterface;
use App\Entity\Ingredients\Fromages\Mozarella;
use App\Entity\Ingredients\Legumes\Aubergine;
use App\Entity\Ingredients\Legumes\Epinard;
use App\Entity\Ingredients\Legumes\OlivesNoires;
use App\Entity\Ingredients\Moules\MouleInterface;
use App\Entity\Ingredients\Moules\MoulesSurgelees;
use App\Entity\Ingredients\Pates\PateEpaisse;
use App\Entity\Ingredients\Pates\PateInterface;
use App\Entity\Ingredients\Poivrons\PoivronInterface;
use App\Entity\Ingredients\Poivrons\PoivronsEnRondelles;
use App\Entity\Ingredients\Sauces\SauceInterface;
use App\Entity\Ingredients\Sauces\SauceTomate;
use Doctrine\Common\Collections\ArrayCollection;

class FabriqueIngredientsPizzaStrasbourg implements FabriqueIngredientsPizzaInterface
{
    public function creerPate(): PateInterface
    {
        return new PateEpaisse();
    }

    public function creerSauce(): SauceInterface
    {
        return new SauceTomate();
    }

    public function creerFromage(): FromageInterface
    {
        return new Mozarella();
    }

    public function creerLegumes(): ArrayCollection
    {
        return new ArrayCollection([new OlivesNoires(), new Epinard(), new Aubergine()]);
    }

    public function creerPoivrons(): PoivronInterface
    {
        return new PoivronsEnRondelles();
    }

    public function creerMoules(): MouleInterface
    {
        return new MoulesSurgelees();
    }
}