<?php

namespace App\Entity;

class PizzaFruitsDeMer extends AbstractPizza implements PizzaInterface
{
    public function __construct()
    {
        parent::__construct();

        $this->nom = 'Pizza fruits de mer';
    }

    public function preparer(): void
    {
        echo "<p>Préparation de $this->nom<p/>";

        $this->pate = $this->fabriqueIngredients->creerPate();
        $this->sauce = $this->fabriqueIngredients->creerSauce();
        $this->fromage = $this->fabriqueIngredients->creerFromage();
        $this->moules = $this->fabriqueIngredients->creerMoules();
    }

    public function isTypeMatch(string $type): bool
    {
        return $type === 'fruitsDeMer';
    }
}