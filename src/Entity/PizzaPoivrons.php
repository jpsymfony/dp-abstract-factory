<?php

namespace App\Entity;

class PizzaPoivrons extends AbstractPizza implements PizzaInterface
{
    public function __construct()
    {
        parent::__construct();

        $this->nom = 'Pizza poivrons';
    }

    public function preparer(): void
    {
        echo "<p>Préparation de $this->nom<p/>";

        $this->pate = $this->fabriqueIngredients->creerPate();
        $this->sauce = $this->fabriqueIngredients->creerSauce();
        $this->fromage = $this->fabriqueIngredients->creerFromage();
        $this->legumes = $this->fabriqueIngredients->creerLegumes();
        $this->poivrons = $this->fabriqueIngredients->creerPoivrons();
    }

    public function isTypeMatch(string $type): bool
    {
        return $type === 'poivrons';
    }
}