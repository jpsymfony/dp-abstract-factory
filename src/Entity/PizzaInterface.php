<?php

namespace App\Entity;

use App\Entity\Ingredients\Fromages\FromageInterface;
use App\Entity\Ingredients\Moules\MouleInterface;
use App\Entity\Ingredients\Pates\PateInterface;
use App\Entity\Ingredients\Poivrons\PoivronInterface;
use App\Entity\Ingredients\Sauces\SauceInterface;
use App\Factory\FabriqueIngredientsPizzaInterface;
use Doctrine\Common\Collections\ArrayCollection;

interface PizzaInterface
{
    public function isTypeMatch(string $type): bool;

    public function getNom(): string;

    public function setNom(string $nom): PizzaInterface;

    public function getPate(): PateInterface;

    public function getSauce(): SauceInterface;

    public function getLegumes(): ArrayCollection;

    public function getFromage(): FromageInterface;

    public function getPoivron(): PoivronInterface;

    public function getMoules(): MouleInterface;

    public function preparer(): void;

    public function cuire(): void;

    public function couper(): void;

    public function emballer(): void;

    /**
     * @return FabriqueIngredientsPizzaInterface
     */
    public function getFabriqueIngredients(): FabriqueIngredientsPizzaInterface;

    /**
     * @param FabriqueIngredientsPizzaInterface $fabriqueIngredients
     */
    public function setFabriqueIngredients(FabriqueIngredientsPizzaInterface $fabriqueIngredients): PizzaInterface;
}
