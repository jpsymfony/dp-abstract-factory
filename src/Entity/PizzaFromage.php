<?php

namespace App\Entity;

class PizzaFromage extends AbstractPizza implements PizzaInterface
{
    public function __construct()
    {
        parent::__construct();

        $this->nom = 'Pizza fromage';
    }

    public function preparer(): void
    {
        echo "<p>Préparation de $this->nom</p>";

        $this->pate = $this->fabriqueIngredients->creerPate();
        $this->sauce = $this->fabriqueIngredients->creerSauce();
        $this->fromage = $this->fabriqueIngredients->creerFromage();
    }

    public function isTypeMatch(string $type): bool
    {
        return $type === 'fromage';
    }
}