<?php

Namespace App\Entity\Ingredients\Legumes;

interface LegumeInterface
{
    public function __toString();
}