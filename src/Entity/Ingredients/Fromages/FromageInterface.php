<?php

Namespace App\Entity\Ingredients\Fromages;

interface FromageInterface
{
    public function __toString();
}