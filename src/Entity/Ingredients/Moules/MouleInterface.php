<?php

Namespace App\Entity\Ingredients\Moules;

interface MouleInterface
{
    public function __toString();
}