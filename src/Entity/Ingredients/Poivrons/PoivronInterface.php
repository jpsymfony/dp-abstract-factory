<?php

Namespace App\Entity\Ingredients\Poivrons;

interface PoivronInterface
{
    public function __toString();
}