<?php

Namespace App\Entity\Ingredients\Sauces;

interface SauceInterface
{
    public function __toString();
}