<?php

namespace App\Entity;

use App\Entity\Ingredients\Fromages\FromageInterface;
use App\Entity\Ingredients\Moules\MouleInterface;
use App\Entity\Ingredients\Pates\PateInterface;
use App\Entity\Ingredients\Poivrons\PoivronInterface;
use App\Entity\Ingredients\Sauces\SauceInterface;
use App\Factory\FabriqueIngredientsPizzaInterface;
use Doctrine\Common\Collections\ArrayCollection;

abstract class AbstractPizza
{
    public function __construct()
    {
        $this->legumes = new ArrayCollection();
    }

    protected string $nom;
    protected PateInterface $pate;
    protected SauceInterface $sauce;
    protected ArrayCollection $legumes;
    protected FromageInterface $fromage;
    protected PoivronInterface $poivron;
    protected MouleInterface $moules;
    protected FabriqueIngredientsPizzaInterface $fabriqueIngredients;

    /**
     * @return string
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     *
     * @return PizzaInterface
     */
    public function setNom(string $nom): PizzaInterface
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return PateInterface
     */
    public function getPate(): PateInterface
    {
        return $this->pate;
    }

    /**
     * @param PateInterface $pate
     * @return self
     */
    public function setPate(PateInterface $pate): PizzaInterface
    {
        $this->pate = $pate;

        return $this;
    }

    /**
     * @return SauceInterface
     */
    public function getSauce(): SauceInterface
    {
        return $this->sauce;
    }

    /**
     * @param SauceInterface $sauce
     * @return self
     */
    public function setSauce(SauceInterface $sauce): PizzaInterface
    {
        $this->sauce = $sauce;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getLegumes(): ArrayCollection
    {
        return $this->legumes;
    }

    /**
     * @param ArrayCollection $legumes
     * @return self
     */
    public function setLegumes(ArrayCollection $legumes): PizzaInterface
    {
        $this->legumes = $legumes;

        return $this;
    }

    /**
     * @return FromageInterface
     */
    public function getFromage(): FromageInterface
    {
        return $this->fromage;
    }

    /**
     * @param FromageInterface $fromage
     * @return self
     */
    public function setFromage(FromageInterface $fromage): PizzaInterface
    {
        $this->fromage = $fromage;

        return $this;
    }

    /**
     * @return PoivronInterface
     */
    public function getPoivron(): PoivronInterface
    {
        return $this->poivron;
    }

    /**
     * @param PoivronInterface $poivron
     * @return self
     */
    public function setPoivron(PoivronInterface $poivron): PizzaInterface
    {
        $this->poivron = $poivron;

        return $this;
    }

    /**
     * @return MouleInterface
     */
    public function getMoules(): MouleInterface
    {
        return $this->moules;
    }

    /**
     * @param MouleInterface $moules
     * @return self
     */
    public function setMoules(MouleInterface $moules): PizzaInterface
    {
        $this->moules = $moules;

        return $this;
    }

    public abstract function isTypeMatch(string $name): bool;

    public abstract function preparer(): void;

    public function cuire(): void
    {
        echo "Cuisson 25 minutes à 180° C<br/>";
    }

    public function couper(): void
    {
        echo "Découpage en parts triangulaires<br/>";
    }

    public function emballer(): void
    {
        echo "Emballage dans une boîte officielle<br/>";
    }

    /**
     * @return FabriqueIngredientsPizzaInterface
     */
    public function getFabriqueIngredients(): FabriqueIngredientsPizzaInterface
    {
        return $this->fabriqueIngredients;
    }

    /**
     * @param FabriqueIngredientsPizzaInterface $fabriqueIngredients
     *
     * @return self
     */
    public function setFabriqueIngredients(FabriqueIngredientsPizzaInterface $fabriqueIngredients): PizzaInterface
    {
        $this->fabriqueIngredients = $fabriqueIngredients;

        return $this;
    }

    public function __toString()
    {
        $result = sprintf('---- %s ----', $this->nom) . "<br>";

        if (!empty($this->pate)) {
            $result .= $this->pate . "<br>";
        }

        if (!empty($this->sauce)) {
            $result .= $this->sauce . "<br>";
        }

        if (!empty($this->fromage)) {
            $result .= $this->fromage . "<br>";
        }

        if ($this->legumes->count()) {
            $result .= implode(', ', $this->legumes->getValues());
        }

        if (!empty($this->moules)) {
            $result .= $this->moules . "<br>";
        }

        if (!empty($this->poivron)) {
            $result .= $this->poivron . "<br>";
        }

        $result .= "<hr>";

        return $result;
    }
}